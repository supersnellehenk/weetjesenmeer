<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

function sendMail($mail, $txt_weetje, $read_more)
{
    $email = htmlspecialchars($mail);
    $weetje = htmlspecialchars($txt_weetje);
    if ($read_more !== null) {
        $inhoud = "
    Inzender: $email,<br>
    Weetje: $weetje,<br>
    Lees Meer: $read_more
    ";
    } else {
        $inhoud = "
    Inzender: $email,<br>
    Weetje: $weetje
    ";
    }

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = 'phpmailfunction2@gmail.com';
    $mail->Password = 'projectenper3';
    $mail->setFrom('phpmailfunction2@gmail.com', 'Mailer');
    $mail->addAddress('phpmailfunction2@gmail.com', 'Ontvanger');
    $mail->Subject = "Weetje van $email";
    $mail->msgHTML($inhoud);
    $mail->AltBody = 'HTML messaging not supported';

    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}

function sendContactMail($firstname, $insertion, $lastname, $mobile_number, $email, $message)
{
    $inhoud = "
    Voornaam: $firstname,<br>
    Tussenvoegsel: $insertion,<br>
    Achternaam: $lastname,<br>
    Telefoonnummer: $mobile_number,<br>
    Email: $email,<br>
    Bericht: $message";

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = 'phpmailfunction2@gmail.com';
    $mail->Password = 'projectenper3';
    $mail->setFrom('phpmailfunction2@gmail.com', 'Mailer');
    $mail->addAddress('phpmailfunction2@gmail.com', 'Ontvanger');
    $mail->Subject = "Weetje van $email";
    $mail->msgHTML($inhoud);
    $mail->AltBody = 'HTML messaging not supported';

    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}

function sendDeleteMail($mail, $inhoud)
{
    $email = $mail;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = 'phpmailfunction2@gmail.com';
    $mail->Password = 'projectenper3';
    $mail->setFrom('phpmailfunction2@gmail.com', 'Info over uw weetje');
    $mail->addAddress($email, 'Ontvanger');
    $mail->Subject = "Weetje van $email";
    $mail->msgHTML($inhoud. '<br>Met vriendelijke groet, <br> administrators van KnowItAll');
    $mail->AltBody = 'HTML messaging not supported';

    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}
