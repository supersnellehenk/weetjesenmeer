<?php
function login ($conn, $usrname, $password) {
    $username = mysqli_real_escape_string($conn, $usrname);
    $userSQL = 'SELECT user_ID, username, password, role FROM users';
    $result = mysqli_query($conn, $userSQL);
    if ($result) {
        $numrows = mysqli_num_rows($result);
        if ($numrows !== 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if ($username === $row['username'] && password_verify($password, $row['password'])) {
                    $_SESSION['user'] = $username;
                    $_SESSION['user_ID'] = $row['user_ID'];
                    $_SESSION['role'] = $row['role'];
                    header('Location: ../../index.php');
                }
            }
        }
    } else {
        echo mysqli_error($conn);
    }
}

function register ($conn, $usrname, $psw, $secondPassword) {
    $username = mysqli_real_escape_string($conn, $usrname);
    $password = password_hash($psw, PASSWORD_DEFAULT);
    $usernameExists = false;
    $userSQL = 'SELECT username FROM users';
    $result = mysqli_query($conn, $userSQL);
    if ($result) {
        $numrows = mysqli_num_rows($result);
        if ($numrows !== 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if ($username === $row['username']) {
                    $usernameExists = true;
                }
            }
        }
        if (!$usernameExists && password_verify($secondPassword, $password)) {
            $sql = "INSERT INTO `users` (username, password) VALUES ('$username', '$password')";
            if (!mysqli_query($conn, $sql)) {
                header("refresh:6; url=../../index.php");
                echo mysqli_error($conn);
            } else {
                header('Location: inloggen.php');
            }
        }
    } else {
        echo mysqli_error($conn);
    }
}
