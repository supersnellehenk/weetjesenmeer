<?php
include('../scripts/connection.php');
include '../scripts/sendmail.php';
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'admin') {
        header('Location: ../../index.php');
    }
} else {
    header('Location: ../../index.php');
}

$regels = [];

if (isset($_POST["submit"])) {
    if (isset($_POST['temp_ID'])) {
        $temp_ID = mysqli_real_escape_string($conn, $_POST['temp_ID']);
    }
    $user_ID = mysqli_real_escape_string($conn, $_POST['user_ID']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $fact_text = mysqli_real_escape_string($conn, $_POST['fact_text']);
    if (isset($_POST['read_more'])) {
        $read_more = mysqli_real_escape_string($conn, $_POST['read_more']);
    }
    if (isset($_POST['date'])) {
        $date = mysqli_real_escape_string($conn, $_POST['date']);
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($_SESSION['user'])) {
    print $_SESSION['user'] . ' - ';
} ?> KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
</head>
<body>
<?php include('components/header.php'); ?>
<div class="container">
    <nav class="navigatie">
        <ul>
            <li>Temp Tabel</li>
            <li><a href="adminfacts.php">Naar Facts Tabel</a></li>
            <li><a href="adminusers.php">Naar Users Tabel</a></li>
			<li><a href="adminToggle.php">Zet Anoniem Indienen Aan/Uit</a></li>
            <li><a href="<?= $_SERVER["PHP_SELF"]; ?>?target=lijst">Lijst</a></li>
        </ul>
    </nav>

    <div class="admincontainer">
        <?php
        if (isset($_POST["submit"])) {
            switch ($_POST["target"]) {
                case "aanpassen":
                    $sql = "UPDATE temp SET temp_ID=$temp_ID, user_ID=$user_ID, email='$email', fact_text='$fact_text', read_more='$read_more',
 date='$date' WHERE temp_ID=$temp_ID";
                    if (!mysqli_query($conn, $sql)) {
                        ?>
                        <p>Error: <?= $sql; ?></p><br>
                        <p><?= mysqli_error($conn); ?></p>
                    <?php
                    } else {
                        ?>
                        <p>Query succesvol.</p>
                    <?php
                    }
                    break;
            }
        }
        if (isset($_GET["target"])) {
            switch ($_GET["target"]) {
                case "lijst":
                    $feedback = mysqli_query($conn, "SELECT * FROM temp");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            foreach ($row as $key => $value) {
                                $regels[$i][$key] = $value;
                            }
                        } ?>
                        <table class="blueTable">
                            <thead>
                            <tr class="kolommen">
                                <th>temp_ID</th>
                                <th>user_ID</th>
                                <th>email</th>
                                <th>fact_text</th>
                                <th>read_more</th>
                                <th>date</th>
                                <th>image</th>
                                <th>aanpassen</th>
                                <th>goedkeuren</th>
                                <th>afkeuren</th>
                            </tr>
                            </thead>
                            <?php
                            $countRegels = count($regels);
                        for ($i = 0; $i < $countRegels; $i++) {
                            ?>
                                <tr>
                                    <?php
                                    foreach ($regels[$i] as $key => $value) {
                                        ?>
                                        <td class="table"><?= $value; ?></td>
                                    <?php
                                    } ?>
                                    <td><a href='admintemp.php?target=aanpassen&ID=<?= $regels[$i]["temp_ID"] ?>'>
                                            <img src="../../images/edit.png" class="img_addordelete"></a>
                                    </td>
                                    <td><a href='admintemp.php?target=add&ID=<?= $regels[$i]["temp_ID"] ?>'>
                                            <img src="../../images/add.png" class="img_addordelete"></a>
                                    </td>
                                    <!-- href='admintemp.php?target=verwijderen&ID= //$regels[$i]["temp_ID"] ' -->
                                    <td><a href=''>
                                            <img src="../../images/delete.png" class="img_addordelete"
                                                 onclick="return confirmDelete(<?= $regels[$i]['temp_ID'] ?>);"></a>
                                    </td>
                                </tr>
                            <?php
                        } ?>
                        </table>
                        <br><br>
                    <?php
                    }
                    break;
                case "aanpassen":
                    $feedback = mysqli_query($conn, "SELECT * FROM temp WHERE temp_ID = '" . $_GET["ID"] . "'");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            $huidigeRow = $row["temp_ID"] - 1;
                            foreach ($row as $key => $value) {
                                $regels[$huidigeRow][$key] = $value;
                                //var_dump($row);
                            }
                        } ?>
                        <form action='<?= $_SERVER["PHP_SELF"]; ?>' method='post'>
                        <table>
                        <?php
                        foreach ($regels as $key => $value) {
                            foreach ($value as $key2 => $value2) {
                                ?>
                                <tr>
                                    <td><?= $key2; ?></td>
                                    <?php ?>
                                    <td><input type='text' value='<?= $value2; ?>'
                                               name='<?= $key2; ?>'>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    }
                    ?>
                    <tr>
                        <td><input type='submit' name='submit' value='Submit'>
                        </td>
                    </tr>
                    </table>
                    <input type='hidden' value='aanpassen' name='target'>
                    </form>
                    <?php
                    break;
                    case 'feedback': ?>
					<form action="<?= $_SERVER['PHP_SELF'] ?>?target=verwijderen&ID=<?= $_GET['ID'] ?>" method="post">
						<textarea name="feedbacktextarea" id="tempDeleteText" cols="30" rows="10"></textarea>
						<input type="submit" name="feedbacksubmit">
					</form>
<?php
                    break;
                case
                "verwijderen":
				$sql1 = 'SELECT email FROM temp WHERE temp_ID = ' . (int)$_GET['ID'];
				$result = mysqli_query($conn, $sql1);
				if ($result) {
					$row = mysqli_fetch_assoc($result);
					$dbemail = $row['email'];
				}
                    $sql = "DELETE FROM temp WHERE temp_ID = " . (int)$_GET["ID"];
                    if (!mysqli_query($conn, $sql)) {
                        ?>
                        <p>Error: <?= $sql; ?><br>
                        <?= mysqli_error($conn); ?>
                    <?php
                    } else {
                        header('Location: admintemp.php?target=lijst');
                    }
                    if (isset($_POST['feedbacksubmit']) && strlen($dbemail) > 0) {
						sendDeleteMail($dbemail, $_POST['feedbacktextarea']);
                    }
                    break;

                case
                "add":
                    $sql = "SELECT * FROM temp WHERE temp_ID=" . (int)$_GET['ID'] . "";
                    $result = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_assoc($result)) {
                        $userid = $row['user_ID'];
                        $facttext = $row['fact_text'];
                        $readmore = $row['read_more'];
                        $dateRow = $row['date'];
                        $email = $row['email'];
                    }

                    if ($conn->query($sql)) {
                        $sqldelete = "DELETE FROM temp WHERE temp_ID=" . (int)$_GET['ID'];
                        if ($conn->query($sqldelete)) {
                            echo "<br>Query succesvol";
							sendDeleteMail($email, 'Uw weetje is goedgekeurd.');
                        } else {
                            echo "Error: " . $sql . "<br>" . $conn->error . "<br>Neem contact op met de beheerder";
                        }
                    } else {
                        echo "Error: " . $sql . "<br>" . $conn->error . "<br>Neem contact op met de beheerder";
                    }


                    // Eerst alle gegevens uit de temp tabel ophalen
                    // Dan heb je alle gegevens in een variabele $results bv
                    // Nieuwe insert query schrijven waarbij je alle data weer in de nieuwe tabel plaatst
                    // Verwijder je de oude gegevens uit de temp tabel
                    break;
            }
        }
        ?>
    </div>
</div>
<?php include('components/footer.php'); ?>
<script>
    function confirmDelete(id) {
        if (confirm("Weet u zeker dat u deze rij wilt verwijderen?")) {
            window.location.replace('admintemp.php?target=feedback&ID=' + id);
        }
        return false;
    }
</script>
</body>
</html>
