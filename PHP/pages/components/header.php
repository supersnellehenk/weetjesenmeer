<?php
//Nederlands
setlocale(LC_TIME, "nld_NLD");
$ned = strftime("%e %B %Y");

$path = $_SERVER['PHP_SELF'];

if (strpos($path, 'index.php') !== false) {
    $path = 'index.php';
}

if ($path !== 'index.php') {
    $stringPrefix = '';
    $imagePrefix = '../../';
    $homePrefix = '../../';
    $scriptPrefix = '../scripts/';
} else {
    $stringPrefix = 'PHP/pages/';
    $imagePrefix = '';
    $homePrefix = '';
    $scriptPrefix = 'PHP/scripts/';
}
?>
<header>
    <div class="header">
        <div class="topimg">
            <a href="<?= $homePrefix; ?>index.php">
                <!--                <img class="jreslogo" src="-->
                <? //= $imagePrefix; ?><!--images/windows_logo_JRES.png">-->
            </a>
        </div>
        <div class="datum"><?= "Vandaag is het: " . $ned; ?></div>
        <img class="login" <?php if (isset($_SESSION['user'])) { ?>
            src="<?= $imagePrefix; ?>images/logout.png"
        <?php } else { ?>
            src="<?= $imagePrefix; ?>images/login.png"
        <?php } ?>
        >
        <?php if (isset($_SESSION['user'])) { ?>
            <p class="headertext"><a href="<?= $scriptPrefix; ?>uitloggen.php">Uitloggen</a>
            <?php if (isset($_SESSION['role']) && $_SESSION['role'] === 'admin') { ?>
                <p class="headertext"><a href="<?= $stringPrefix; ?>admin.php">Admin Paneel</a></p>
            <?php } ?>
            <?php if (isset($_SESSION['role']) && $_SESSION['role'] === 'user') { ?>
                <p class="headertext"><a href="<?= $stringPrefix; ?>usersweetjes.php">Je Weetjes</a>
            <?php }
        } else { ?>
            <p class="headertext"><a href="<?= $stringPrefix; ?>inloggen.php">Inloggen</a></p>
			<p class="headertext"><a href="<?= $stringPrefix; ?>registreren.php">Registreren</a></p>
        <?php } ?>
    </div>
    <div class="nav">
        <p class="linkscss"><a href="<?= $homePrefix; ?>index.php">Home</a></p>
        <p class="linkscss"><a href="<?= $stringPrefix; ?>weetje_indienen.php">Weetjes indienen? Klik hier!</a></p>
        <p class="linkscss"><a href="<?= $stringPrefix; ?>datumweetje.php">Weetje per datum</a></p>
        <p class="linkscss"><a href="<?= $stringPrefix; ?>contact.php">Contact</a></p>
    </div>

</header>
