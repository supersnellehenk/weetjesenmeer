<?php
$path = $_SERVER['PHP_SELF'];

if (strpos($path, 'index.php') !== false) {
    $path = 'index.php';
}

if ($path !== 'index.php') {
    $imagePrefix = '../../';
} else {
    $imagePrefix = '';
}
?>
<footer>
    <div id="text_footer">
        Voor updates en meer coole weetjes<br>
        volg je ons op Facebook, Instagram en Twitter!
    </div>

    <div id="plugins_footer">
        <a href="https://twitter.com/JRES38482971/" target="_blank">
            <img src="<?= $imagePrefix ?>images/twitter-logo.png" alt="Twitter icon" class="pluginicons">
        </a>

        <a href="https://www.facebook.com/JRES-207080370104511/" target="_blank">
            <img src="<?= $imagePrefix ?>images/facebook-logo.png" alt="Facebook icon" class="pluginicons">
        </a>

        <a href="https://www.instagram.com/jres123456/" target="_blank">
            <img src="<?= $imagePrefix ?>images/instagram-logo.png" alt="Instagram icon" class="pluginicons">
        </a>
    </div>

    <div id="text_copyright">
        KnowItAll-techweetjes.nl 2018 ©
    </div>
</footer>