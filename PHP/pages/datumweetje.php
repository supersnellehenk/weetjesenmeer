<?php
include('../scripts/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php if (isset($_SESSION['user'])) {
    print $_SESSION['user'] . ' - ';
} ?>Datum Weetje - KnowItAll</title>
    <link rel="stylesheet" href="../../css/datumweetje.css">
    <link rel="stylesheet" href="../../css/styles.css">
    <link rel="stylesheet" href="../../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="../../css/jquery-ui.theme.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(function () {
            $.datepicker.regional['nl'] = {
                closeText: "Sluiten",
                prevText: "←",
                nextText: "→",
                currentText: "Vandaag",
                monthNames: ["januari", "februari", "maart", "april", "mei", "juni",
                    "juli", "augustus", "september", "oktober", "november", "december"],
                monthNamesShort: ["jan", "feb", "mrt", "apr", "mei", "jun",
                    "jul", "aug", "sep", "okt", "nov", "dec"],
                dayNames: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
                dayNamesShort: ["zon", "maa", "din", "woe", "don", "vri", "zat"],
                dayNamesMin: ["zo", "ma", "di", "wo", "do", "vr", "za"],
                weekHeader: "Wk",
                dateFormat: "dd-mm-yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ""
            };
            $.datepicker.setDefaults($.datepicker.regional['nl']);
            $("#datepicker").datepicker({dateFormat: 'mm-dd'});
        });
    </script>
</head>
<body>
<?php include('components/header.php'); ?>
<main>
    <h3 class="datumweetjetext">kies een datum en krijg meer weetjes te zien!</h3>
    <div class="block">
        <?php
        if (isset($_POST['submit'])) {
            $date = mysqli_real_escape_string($conn, $_POST['date']);
            $result = mysqli_query($conn, "SELECT * FROM facts WHERE date LIKE '%$date%'");
            if ($result) {
                $numrows = mysqli_num_rows($result);
                if ($numrows !== 0) {
                    $rowID = 0;
                    while ($row = mysqli_fetch_assoc($result)) {
                        $fact_text[$rowID] = $row['fact_text'];
                        $read_more[$rowID] = $row['read_more'];
                        $dbDate[$rowID] = $row['date'];
                        $dbImage[$rowID] = $row['image'];
                        $rowID++;
                    }
                    $countText = count($fact_text);
                    for ($i = 0; $i < $countText; $i++) {
                        if (isset($fact_text[$i]) && strlen($fact_text[$i]) > 0) {
                            print substr($dbDate[$i], 0, -9) . ' : ' . $fact_text[$i] . '<br>';
                        }
                        if (isset($dbImage[$i]) && strlen($dbImage[$i]) > 0) {
                            ?>
							<img src="<?= $dbImage[$i] ?>" height="80px"><br>
						<?php
                        }
                        if (isset($read_more) && strlen($read_more[$i]) > 0) {
                            ?>
                            <a href="<?= $read_more[$i] ?>" target="_blank">Lees Meer</a><br>
                        <?php
                        }
                    }
                } else {
                    print 'Geen weetje voor deze datum.';
                }
            } else {
                echo mysqli_error($conn);
            }
        }
        ?>
    </div>
    <div class="datumknop">
        <form method="post" action="">
            <input type="submit" name="submit" value="Check Datum"><br>
            <input type="text" name="date" id="datepicker" autocomplete="off" value="<?php if (isset($date)) {
            print $date;
        } ?>"><br>
        </form>
    </div>
</main>
<?php include('components/footer.php'); ?>
</body>
</html>
