<?php
include('../scripts/connection.php');
include('../scripts/authentication.php');
if (isset($_POST['submit'])) {
    login($conn, $_POST['username'], $_POST['password']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php if (isset($_SESSION['user'])) {
            print $_SESSION['user'] . ' - ';
        } ?>Inloggen - KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
</head>
<body>
<?php include('components/header.php'); ?>
<p class="inlogtext">Vul hier de velden in om in te loggen.</p>
<form class="inlogform" action="inloggen.php" method="post">
    <br><label for="username">Gebruikersnaam:</label><br>
    <input type="text" placeholder="Uw gebruikersnaam" name="username" id="username" required><br>
    <label for="password">Wachtwoord:</label><br>
    <input type="password" placeholder="Uw wachtwoord" name="password" id="password" required><br><br>
    <label style="cursor: pointer;">
        <input type="checkbox" onclick="showPassword()">Laat wachtwoord zien<br><br>
    </label>
    <input type="submit" name="submit" value="Inloggen"><br>
</form>
<script>
    function showPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<?php include('components/footer.php'); ?>
</body>
</html>