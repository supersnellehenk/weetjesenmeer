<?php
session_start();
if (isset($_POST['submit'])) {
    include '../scripts/sendmail.php';
    $firstname = htmlspecialchars($_POST['firstname']);
    $insertion = htmlspecialchars($_POST['insertion']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $mobile_number = htmlspecialchars($_POST['mobile_number']);
    $email = htmlspecialchars($_POST['email']);
    $message = htmlspecialchars($_POST['message']);
    sendContactMail($firstname, $insertion, $lastname, $mobile_number, $email, $message);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
</head>
<body>
<?php include 'components/header.php'; ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="registform">
    <input required type="text" name="firstname" placeholder="Voornaam"><br><br>
    <input type="text" name="insertion" placeholder="Tussenvoegsel"><br><br>
    <input required type="text" name="lastname" placeholder="Achternaam"><br><br>
    <input pattern="\d*" required type="text" name="mobile_number" placeholder="Mobiele nummer"><br><br>
    <input required type="email" name="email" placeholder="E-mailadres"><br><br>
    <input type="text" name="message" placeholder="Bericht"><br><br>
    <input required type="submit" name="submit" value="Verzenden"><br><br>
</form>
<?php include 'components/footer.php'; ?>
</body>
</html>