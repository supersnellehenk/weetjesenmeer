<?php
include('../scripts/dbqueries.php');
include('../scripts/connection.php');
include('../scripts/sendmail.php');
ini_set('file_uploads', 'On');
if (isset($_POST['submit'])) {
    //sendMail($_POST['email'], $_POST['txt_weetje'], $_POST['read_more']);
    if (isset($_SESSION['user_ID'])) {
        $user_ID = $_SESSION['user_ID'];
    } else {
        $user_ID = 0;
    }
    if ($_FILES['imageFile']['error'] === 0) {
        $target_dir = "uploads/";
        $uploadOk = 1;
        $extHelp = $target_dir . basename($_FILES["imageFile"]["name"]);
        $imageFileType = strtolower(pathinfo($extHelp, PATHINFO_EXTENSION));
        $ext = substr($_FILES['imageFile']['name'], -4);
        $md5dFile = md5(str_replace($ext, '', $_FILES['imageFile']['name']));
        $target_file = $target_dir . $md5dFile . $ext;
// Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            //echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["imageFile"]["size"] > 500000) {
            //echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType !== "jpg" && $imageFileType !== "png" && $imageFileType !== "jpeg"
            && $imageFileType !== "gif") {
            //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk === 0) {
            //echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
                //echo "The file ". basename($_FILES["imageFile"]["name"]). " has been uploaded.";
            } else {
                //echo "Sorry, there was an error uploading your file.";
            }
        }
    } else {
        $target_file = 'uploads/default.png';
    }
    $tempdate = explode('-', $_POST['date']);
    $date = $tempdate[0] . '-' . $tempdate[1] . '-' . $tempdate[2];
    insertFact($user_ID, $_POST['email'], $_POST['txt_weetje'], $_POST['read_more'], $date, $target_file, $conn);
}

$anoniemSQL = 'SELECT * FROM anoniem';
$result = mysqli_query($conn, $anoniemSQL);
$row = mysqli_fetch_assoc($result);
$anoniem = $row['anoniem'];
if ($anoniem === '0') {
	$anoniemToggle = 'required';
} else {
	$anoniemToggle = '';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php if (isset($_SESSION['user'])) {
            print $_SESSION['user'] . ' - ';
        } ?>Weetje indienen - KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
</head>

<body>
<?php include('components/header.php'); ?>

<br>
<div id="sf_text">
    Dien een weetje in
</div>
<br><br>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" id="formtag" name="formtag"
      enctype="multipart/form-data" onsubmit="return checkRole('<?php if (isset($_SESSION['role'])) { echo $_SESSION['role'];} ?>');">
    <fieldset id="sf_form_fieldset">
        <div id="sf_elementscontainer">
            <input <?= $anoniemToggle ?> type="email" name="email" placeholder="E-mailadres"><br><br>
            <textarea required name="txt_weetje" placeholder="Weetje"></textarea><br><br>
            <input type="text" name="read_more" placeholder="Lees meer link"><br><br>
            <input required type="date" name="date"><br><br>
            <input type="file" name="imageFile"><br><br>
            <input type="submit" value="Indienen" id="sf_submit_button" name="submit">
        </div>
    </fieldset>
</form>
<?php include('components/footer.php'); ?>
<script>
	function checkRole(role) {
		if (role === 'banned') {
			return false;
		}
		return true;
	}
</script>
</body>
</html>
