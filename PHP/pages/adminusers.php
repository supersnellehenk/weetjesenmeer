<?php
include('../scripts/connection.php');
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'admin')
        header('Location: ../../index.php');
} else {
    header('Location: ../../index.php');
}

$regels = [];

if (isset($_POST["submit"])) {
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    if (isset($_POST['2ndpassword'])) {
        $secondPassword = mysqli_real_escape_string($conn, $_POST['2ndpassword']);
    }
    if (isset($_POST['user_ID'])) {
        $user_ID = $_POST['user_ID'];
    }
    if (isset($_POST['role'])) {
        $role = mysqli_real_escape_string($conn, $_POST['role']);
    } else {
        $role = '';
    }
}
?>
<!DOCTYPE html>
<html>
<head><title><?php if (isset($_SESSION['user'])) {
        print $_SESSION['user'] . ' - ';
    } ?>
     KnowItAll</title>

    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
</head>
<body>
<?php include('components/header.php'); ?>
<div class="container">
    <nav class="navigatie">
        <ul>
            <li>Users Tabel</li>
            <li><a href="adminfacts.php">Naar Facts Tabel</a></li>
            <li><a href="admintemp.php">Naar Temp Tabel</a></li>
			<li><a href="adminToggle.php">Zet Anoniem Indienen Aan/Uit</a></li>
            <li><a href="<?= $_SERVER["PHP_SELF"]; ?>?target=lijst">Lijst</a></li>
            <li><a href="<?= $_SERVER["PHP_SELF"]; ?>?target=invoeren">Invoeren</a></li>
        </ul>
    </nav>
    <div class="admincontainer">
        <?php
        if (isset($_POST["submit"])) {
            switch ($_POST["target"]) {
                case "insert":
                    if ($password === $secondPassword) {
                        $password = hash('sha256', $secondPassword);
                        $sql = "INSERT INTO users (username, password, role) VALUES('$username', '$password', '$role')";
                        if (!mysqli_query($conn, $sql)) { ?>
                            <p>Error: <?= $sql; ?></p><br>
                            <p><?= mysqli_error($conn); ?></p>
                        <?php }
                    } else { ?>
                        <p>Wachtwoorden komen niet overeen.</p>
                    <?php }
                    break;
                case "aanpassen":
                    $sql = "UPDATE users SET username='$username', password='$password', role='$role'
 WHERE user_ID=$user_ID";
                    if (!mysqli_query($conn, $sql)) { ?>
                        <p>Error: <?= $sql; ?></p><br>
                        <p><?= mysqli_error($conn); ?></p>
                    <?php } else { ?>
                        <p>Query succesvol.</p>
                    <?php }
                    break;
            }
        }
        if (isset($_GET["target"])) {
            switch ($_GET["target"]) {
                case "invoeren":
                    ?>
                    <form action="<?= $_SERVER["PHP_SELF"]; ?>" method="post">
                        <table>
                            <thead>
                            <tr>
                                <th><input type="submit" name="submit" value="Invoeren"></th>
                                <th><input type="reset" value="Reset"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><label for="username">Gebruikersnaam</label></td>
                                <td><input type="text" id="username" name="username" required></td>
                            </tr>
                            <tr>
                                <td><label for="password">Wachtwoord</label></td>
                                <td><input type="password" id="password" name="password" required></td>
                            </tr>
                            <tr>
                                <td><label for="2ndpassword">Wachtwoord Herhalen</label></td>
                                <td><input type="password" id="2ndpassword" name="2ndpassword" required></td>
                            </tr>
                            <tr>
                                <td><label for="role">Rol</label></td>
                                <td><input type="text" id="role" name="role"></td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="target" value="insert">
                    </form>
                    <?php break;
                case "lijst":
                    $feedback = mysqli_query($conn, "SELECT user_ID, username, role FROM users");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            foreach ($row as $key => $value) {
                                $regels[$i][$key] = $value;
                            }
                        } ?>
                        <table class="blueTable">
                            <thead>
                            <tr>
                                <th>User_ID</th>
                                <th>username</th>
                                <th>role</th>
                                <th>aanpassen</th>
                                <th>verwijderen</th>
                            </tr>
                            </thead>
                            <?php
                            $countRegels = count($regels);
                            for ($i = 0; $i < $countRegels; $i++) { ?>
                                <tr>
                                    <?php
                                    foreach ($regels[$i] as $key => $value) { ?>
                                        <td><?= $value; ?></td>
                                    <?php } ?>
                                    <td><a href='adminusers.php?target=aanpassen&ID=<?= $regels[$i]["user_ID"] ?>'>
                                            Aanpassen</a>
                                    </td>
                                    <td><a href='adminusers.php?target=verwijderen&ID=<?= $regels[$i]["user_ID"] ?>'>
                                            Verwijderen</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <br>
                    <?php }
                    break;
                case "aanpassen":
                    $feedback = mysqli_query($conn, "SELECT * FROM users WHERE user_ID = '" . $_GET["ID"] . "'");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            $huidigeRow = $row["user_ID"] - 1;
                            foreach ($row as $key => $value) {
                                $regels[$huidigeRow][$key] = $value;
                                //var_dump($row);
                            }
                        } ?>
                        <form action='<?= $_SERVER["PHP_SELF"]; ?>' method='post'>
                            <table>
                                <?php
                                foreach ($regels as $key => $value) {
                                    foreach ($value as $key2 => $value2) { ?>
                                        <tr>
                                        <td><?= $key2; ?></td>
                                        <?php
                                        if ($key2 === 'role') { ?>
                                            <td><select name="<?= $key2 ?>">
                                                    <option value="user"
                                                        <?php if ($value2 === 'user') { ?> selected <?php } ?>
                                                    >User
                                                    </option>
                                                    <option value="admin"
                                                        <?php if ($value2 === 'admin') { ?> selected <?php } ?>
                                                    >Admin
                                                    </option>
                                                    <option value="banned"
                                                        <?php if ($value2 === 'banned') { ?> selected <?php } ?>
                                                    >Banned
                                                    </option>
                                                </select></td>
                                        <?php } else { ?>
                                            <td><input type='text' value='<?= $value2; ?>'
                                                       name='<?= $key2; ?>'>
                                            </td>
                                            </tr>
                                        <?php }
                                    }
                                }
                                ?>
                                <tr>
                                    <td><input type='submit' name='submit' value='Submit'>
                                    </td>
                                </tr>
                            </table>
                            <input type='hidden' value='aanpassen' name='target'>
                        </form>
                    <?php }
                    break;
                case
                "verwijderen":
                    $sql = "DELETE FROM users WHERE user_ID = " . (int)$_GET["ID"];
                    if (!mysqli_query($conn, $sql)) { ?>
                        <p>Error: <?= $sql; ?><br>
                        <?= mysqli_error($conn); ?>
                    <?php } else { ?>
                        <p>Query succesvol.</p>
                    <?php }
                    break;
            }
        }
        ?>
    </div>
</div>
<?php include('components/footer.php'); ?>
</body>
</html>
