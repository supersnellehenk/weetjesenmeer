<?php
include('../scripts/connection.php');
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'admin')
    header('Location: ../../index.php');
} else {
    header('Location: ../../index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($_SESSION['user'])) { print $_SESSION['user'] . ' - '; } ?>Admin - JRES</title>
    <link rel="icon" type="image/png" sizes="32x32" href="../../images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../images/favicon-16x16.png">
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
</head>
<body>
<?php include('components/header.php'); ?>
<div class="adminlinks">
    <p><a href="adminusers.php">Users Tabel</a></p>
    <p><a href="adminfacts.php">Facts Tabel</a></p>
    <p><a href="admintemp.php">Temp Tabel</a></p>
	<p><a href="adminToggle.php">Zet Anoniem Indienen Aan/Uit</a></p>
</div>
<?php include('components/footer.php'); ?>
</body>
</html>
