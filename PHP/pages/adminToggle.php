<?php
include '../scripts/connection.php';
if (isset($_POST['submit'])) {
    mysqli_query($conn, 'UPDATE anoniem SET anoniem = ' . $_POST['anonToggle']);
}
$result = mysqli_query($conn, 'SELECT * FROM anoniem');
$row = mysqli_fetch_assoc($result);
$anonWaarde = $row['anoniem'];

if ($anonWaarde === '0') {
    $anonText = 'Zet anoniem inleveren uit';
    $anonToggle = '1';
} else {
    $anonText = 'Zet anoniem inleveren aan';
    $anonToggle = '0';
}
?>
<!DOCTYPE html>
<html lang="en">
<head><title><?php if (isset($_SESSION['user'])) {
    print $_SESSION['user'] . ' - ';
} ?>
     KnowItAll</title>

    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
 </head>
<body>
	<?php include '../pages/components/header.php'; ?>
	<nav class="navigatie">
        <ul>
            <li>Zet Anoniem Indienen Aan/Uit</li>
            <li><a href="adminfacts.php">Naar Facts Tabel</a></li>
            <li><a href="admintemp.php">Naar Temp Tabel</a></li>
			<li><a href="adminusers.php">Naar Users Tabel</a></li>
        </ul>
    </nav>
	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" class="toggleForm">
		<input type="submit" name="submit" value="<?= $anonText ?>">
		<input type="hidden" name="anonToggle" value="<?= $anonToggle ?>">
	</form>
	<?php include '../pages/components/footer.php'; ?>
</body>
</html>
