<?php
include('../scripts/connection.php');
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'admin')
        header('Location: ../../index.php');
} else {
    header('Location: ../../index.php');
}

$regels = [];

if (isset($_POST["submit"])) {
    if (isset($_POST['fact_ID'])) {
        $fact_ID = mysqli_real_escape_string($conn, $_POST['fact_ID']);
    }
    $user_ID = mysqli_real_escape_string($conn, $_POST['user_ID']);
    $fact_text = mysqli_real_escape_string($conn, $_POST['fact_text']);
    if (isset($_POST['date'])) {
        $date = mysqli_real_escape_string($conn, $_POST['date']);
    }
    if (isset($_POST['read_more'])) {
        $read_more = mysqli_real_escape_string($conn, $_POST['read_more']);
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($_SESSION['user'])) {
            print $_SESSION['user'] . ' - ';
        } ?> KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
</head>
<body>
<?php include('components/header.php'); ?>
<div class="container">
    <nav class="navigatie">
        <ul>
            <li>Facts Tabel</li>
            <li><a href="adminusers.php">Naar Users Tabel</a></li>
            <li><a href="admintemp.php">Naar Temp Tabel</a></li>
			<li><a href="adminToggle.php">Zet Anoniem Indienen Aan/Uit</a></li>
            <li><a href="<?= $_SERVER["PHP_SELF"]; ?>?target=lijst">Lijst</a></li>
            <li><a href="<?= $_SERVER["PHP_SELF"]; ?>?target=invoeren">Invoeren</a></li>
        </ul>
    </nav>
    <div class="admincontainer">
        <?php
        if (isset($_POST["submit"])) {
            switch ($_POST["target"]) {
                case "insert":
                    if (isset($read_more)) {
                        $sql = "INSERT INTO facts (user_ID, fact_text) VALUES ($user_ID, '$fact_text')";
                    } else {
                        $sql = "INSERT INTO facts (user_ID, fact_text, read_more)
 VALUES ($user_ID, '$fact_text', '$read_more')";
                    }
                    if (!mysqli_query($conn, $sql)) {
                        echo mysqli_error($conn);
                    }
                    break;
                case "aanpassen":
                    $sql = "UPDATE facts SET user_ID='$user_ID', fact_text='$fact_text', read_more='$read_more',
 date='$date' WHERE fact_ID=$fact_ID";
                    if (!mysqli_query($conn, $sql)) { ?>
                        <p>Error: <?= $sql; ?></p><br>
                        <p><?= mysqli_error($conn); ?></p>
                    <?php } else { ?>
                        <p>Query succesvol.</p>
                    <?php }
                    break;
            }
        }
        if (isset($_GET["target"])) {
            switch ($_GET["target"]) {
                case "invoeren":
                    ?>
                    <form action="<?= $_SERVER["PHP_SELF"]; ?>" method="post">
                        <table>
                            <thead>
                            <tr>
                                <th><input type="submit" name="submit" value="Invoeren"></th>
                                <th><input type="reset" value="Reset"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><label for="user_ID">user_ID</label></td>
                                <td><input type="text" id="user_ID" name="user_ID" required></td>
                            </tr>
                            <tr>
                                <td><label for="fact_text">Fact Text</label></td>
                                <td><input type="text" id="fact_text" name="fact_text" required></td>
                            </tr>
                            <tr>
                                <td><label for="read_more">Lees Meer Link</label></td>
                                <td><input type="text" id="read_more" name="read_more"></td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" name="target" value="insert">
                    </form>
                    <?php break;
                case "lijst":
                    $feedback = mysqli_query($conn, "SELECT * FROM facts");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            foreach ($row as $key => $value) {
                                $regels[$i][$key] = $value;
                            }
                        } ?>
                        <table class="blueTable">
                            <thead>
                            <tr class="kolommen">
                                <th>fact_ID</th>
                                <th>user_ID</th>
                                <th>e-mail</th>
                                <th>fact_text</th>
                                <th>read_more</th>
                                <th>date</th>
                                <th>image</th>
                                <th>aanpassen</th>
                                <th>verwijderen</th>
                            </tr>
                            </thead>
                            <?php
                            $countRegels = count($regels);
                            for ($i = 0; $i < $countRegels; $i++) { ?>
                                <tr>
                                    <?php
                                    foreach ($regels[$i] as $key => $value) { ?>
                                        <td class="table"><?= $value; ?></td>
                                    <?php } ?>
                                    <td><a href='adminfacts.php?target=aanpassen&ID=<?= $regels[$i]["fact_ID"] ?>'>
                                            Aanpassen</a>
                                    </td>
                                    <td><a href='adminfacts.php?target=verwijderen&ID=<?= $regels[$i]["fact_ID"] ?>'>
                                            Verwijderen</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <br><br>
                    <?php }
                    break;
                case "aanpassen":
                    $feedback = mysqli_query($conn, "SELECT * FROM facts WHERE fact_ID = '" . $_GET["ID"] . "'");
                    $rows = mysqli_num_rows($feedback);
                    if ($rows > 0) {
                        for ($i = 0; $i < $rows; $i++) {
                            $row = mysqli_fetch_assoc($feedback);
                            $huidigeRow = $row["fact_ID"] - 1;
                            foreach ($row as $key => $value) {
                                $regels[$huidigeRow][$key] = $value;
                                //var_dump($row);
                            }
                        } ?>
                        <form action='<?= $_SERVER["PHP_SELF"]; ?>' method='post'>
                        <table>
                        <?php
                        foreach ($regels as $key => $value) {
                            foreach ($value as $key2 => $value2) { ?>
                                <tr>
                                    <td><?= $key2; ?></td>
                                    <?php ?>
                                    <td><input type='text' value='<?= $value2; ?>'
                                               name='<?= $key2; ?>'>
                                    </td>
                                </tr>
                            <?php }
                        }
                    }
                    ?>
                    <tr>
                        <td><input type='submit' name='submit' value='Submit'>
                        </td>
                    </tr>
                    </table>
                    <input type='hidden' value='aanpassen' name='target'>
                    </form>
                    <?php
                    break;
                case
                "verwijderen":
                    $sql = "DELETE FROM facts WHERE fact_ID = " . (int)$_GET["ID"];
                    if (!mysqli_query($conn, $sql)) { ?>
                        <p>Error: <?= $sql; ?><br>
                        <?= mysqli_error($conn); ?>
                    <?php } else { ?>
                        <p>Query succesvol.</p>
                    <?php }
                    break;
            }
        }
        ?>
    </div>
</div>
<?php include('components/footer.php'); ?>
</body>
</html>
