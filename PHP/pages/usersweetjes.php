<?php
include('../scripts/connection.php');

$regels = [];

if (isset($_POST["submit"])) {
    if (isset($_POST['fact_ID'])) {
        $fact_ID = mysqli_real_escape_string($conn, $_POST['fact_ID']);
    }
    if (isset($_POST['user_ID'])) {
        $user_ID = mysqli_real_escape_string($conn, $_POST['user_ID']);
    }
    $fact_text = mysqli_real_escape_string($conn, $_POST['fact_text']);
    if (isset($_POST['date'])) {
        $date = mysqli_real_escape_string($conn, $_POST['date']);
    }
    if (isset($_POST['read_more'])) {
        $read_more = mysqli_real_escape_string($conn, $_POST['read_more']);
    }
}
?>
<?php
function createUsersweetjes()
{
    $resultrow = '';
    global $conn;
    $sql = "SELECT `fact_text`,`read_more` ,`date` ,`image` FROM `facts` WHERE `user_ID` = '" . $_SESSION['user_ID'] . "'";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->execute();
        $result = $stmt->get_result();
        if ($stmt->affected_rows > 0) {
            $resultrow = "<div class='tabelverwijderen'>" . "<table class='blueTable' align='center'>" .
                "<p class='delete'>Hier staan de weetjes die u heeft ingediend!</p>" . "<p class='delete'>Uw goedgekeurde weetjes:</p>";
            while ($row = $result->fetch_row()) {
                $resultrow .= "<tr>";
                $resultrow .= "<td><b>" . ucfirst($row[0]) . "</td>";
                $resultrow .= "<td>" . $row[1] . "</td>";
                $resultrow .= "<td>" . $row[2] . "</td>";
                $resultrow .= "<td>" . $row[3] . "</td>";
                $resultrow .= "</tr>";
            }
            $resultrow .= "</table>";
        }
        $sql = "SELECT `temp_ID`, `fact_text`,`read_more` ,`date` ,`image` FROM `temp` WHERE `user_ID` = '" . $_SESSION['user_ID'] . "'";
        if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $result = $stmt->get_result();
            if ($stmt->affected_rows > 0) {
                $resultrow .= "<table class='blueTable' align='center'><p class='delete'>Nog goed te keuren weetjes:</p>";
                while ($row = $result->fetch_assoc()) {
                    $resultrow .= "<tr>";
                    $resultrow .= "<td><b>" . ucfirst($row['fact_text']) . "</b></td>";
                    $resultrow .= "<td>" . $row['read_more'] . "</td>";
                    $resultrow .= "<td>" . $row['date'] . "</td>";
                    $resultrow .= "<td>" . $row['image'] . "</td>";
                    $resultrow .= "<td class='deletequery'><p>Aanpassen</p><a class=\"aanpastext\" href=\"" . $_SERVER['PHP_SELF'] . '?target=aanpassen&upd='
                        . $row['temp_ID'] . "\"><img class='img_addordelete' src='../../images/edit.png'></a></td>";
                    $resultrow .= "<td><p>Verwijderen</p><a href=\"" . $_SERVER['PHP_SELF'] . '?target=verwijderen&del='
                        . $row['temp_ID'] . "\"><img class='img_addordelete' src='../../images/delete.png'></a></td>";
                    $resultrow .= "</tr>";
                }
                $resultrow .= "</table>";
            }
            $resultrow .= "</table>" . "</div>";
        } else {
            echo "<p class='geenblogtxt'>" . "U heeft nog geen weetjes gepost , post eens wat leuks!" . "</p>";
        }
    }
    return $resultrow;
}
$regels = [];

if (isset($_GET['target'])) {
    switch ($_GET['target']) {
        case 'verwijderen':
            $persoon_ID = $_GET['del'];
            $SQL = "DELETE FROM temp WHERE temp_ID=?";
            $stmt = $conn->prepare($SQL);
            $stmt->bind_param('s', $persoon_ID);
            if ($stmt->execute()) {
                echo "";
            } else {
                echo "Error tijdens het verwijderen van de record: " . mysqli_error($conn);
            }
            $stmt->close();

            break;

        case 'aanpassen':
            if (isset($_GET['upd'])) {

                $persoon_ID = $_GET['upd'];
                $sql = "SELECT `fact_text`, `read_more`, `date` FROM `temp` WHERE temp_ID=" . intval($_GET['upd']);
                $result = mysqli_query($conn, $sql);

                while ($row = mysqli_fetch_assoc($result)) {
                    $regels[] = '<div class="formaanpassen">' . '<p class="delete">Pas hier de gegevens aan van uw fact!</p> ' . '<form action="' . $_SERVER['PHP_SELF'] . '" method="post" enctype="multipart/form-data">
                De fact:<br> <input type="text" name="fact_text" placeholder="Uw fact" value="' . $row['fact_text'] . '"><br>
                read more link:<br> <input type="text" name="read_more" placeholder="Read more link" value="' . $row['read_more'] . '"><br>
                date:<br> <input type="text" name="date" placeholder="datum" value="' . $row['date'] . '"><br>
                        <input type="hidden" name="target" value="update">
                        <br><br><input type="submit" name="submit" value="Aanpassen">
                        <input type="hidden" name="temp_ID" value="' . $_GET['upd'] . '"><br>
                </form>' . '</div>';
                }

            }
            break;

    }
}

if (isset($_POST['target'])) {

    if ($_POST['target']) {
        $fact_text = htmlentities(mysqli_real_escape_string($conn, ucfirst($_POST['fact_text'])));
        $read_more = htmlentities(mysqli_real_escape_string($conn, ucfirst($_POST['read_more'])));
        $date = htmlentities(mysqli_real_escape_string($conn, ucfirst($_POST['date'])));
        $temp_ID = $_POST['temp_ID'];

        switch ($_POST['target']) {
            case 'update':
				$stmt = $conn->prepare('UPDATE temp SET fact_text=?, read_more=?, date=? WHERE temp_ID=?');
				$stmt->bind_param('sssd', $fact_text, $read_more, $date, $temp_ID);
                if ($stmt->execute()) {
                    echo "<div class='aangepast'>Uw weetje is succesvol aangepast!</div>";
                } else {
					echo '<div class="aangepast">Error tijdens het updaten</div>' . mysqli_error($conn);
                }

                break;

        }
    }
}


?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($_SESSION['user'])) {
            print $_SESSION['user'] . ' - ';
        } ?>Admin - KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
</head>
<body>

<?php include('components/header.php'); ?>
<?php echo createUsersweetjes(); ?>
<br><br>
<?php
if (isset($regels)) {
    foreach ($regels as $key => $regel) {
        echo "<br>$regel";
    }
}
?><br><br>
<?php include('components/footer.php'); ?>
</body>
</html>
