<?php
include('../scripts/connection.php');
include('../scripts/authentication.php');
if (isset($_POST['submit'])) {
    register($conn, $_POST['username'], $_POST['password'], $_POST['2ndpassword']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php if (isset($_SESSION['user'])) {
            print $_SESSION['user'] . ' - ';
        } ?>Registreren - KnowItAll</title>
    <link rel="stylesheet" href="../../css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</head>
<body>
<?php include('components/header.php'); ?>
<p class="registtext">Vul hier de velden in om te registreren</p>
<form class="registform" action="registreren.php" method="post" onsubmit="return verifyForm();">
    <label for="username">Gebruikersnaam:</label><br>
    <input type="text" placeholder="Uw gebruikersnaam" name="username" id="username" onblur="verifyName(this.value);"
           required><br>
    <p id="errorUser">Sorry, gebruikersnaam bestaat al</p>
    <label for="password">Wachtwoord:</label><br>
    <input type="password" placeholder="Uw wachtwoord" name="password" id="password"
           required><br><br>
    <label for="2ndpassword">Wachtwoord herhalen:</label><br>
    <input type="password" placeholder="Herhaal wachtwoord" name="2ndpassword" id="2ndpassword"
           required><br><br>
    <label style="cursor: pointer;">
        <input type="checkbox" onclick="showPassword()">Laat wachtwoord zien<br><br>
    </label>
    <input type="submit" name="submit" value="Registreren">
</form>
<p id="errorMsg"></p>
<script>
    function showPassword() {
        var x = document.getElementById("password");
        var y = document.getElementById("2ndpassword");

        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }

        if (y.type === "password") {
            y.type = "text";
        } else {
            y.type = "password";
        }
    }

    function verifyForm() {
        var pswExists = verifyPassword(document.getElementById('password').value,
            document.getElementById('2ndpassword').value);
        verifyName(document.getElementById('username').value);
        return (pswExists && !userExists);
    }

    function verifyPassword(password, secondPassword) {
        if (password === secondPassword) {
            return true;
        } else {
            document.getElementById('errorMsg').innerHTML = 'Wachtwoorden komen niet overeen.';
            return false;
        }
    }

    function verifyName(name) {
        $.post('../scripts/verifyName.php?name=' + name, function (response) {
            return verifyPost(response);
        });
    }

    var userExists = false;

    function verifyPost(response) {
        if (response === 'true') {
            document.getElementById('username').style.borderColor = 'red';
            document.getElementById('username').style.marginBottom = '0';
            document.getElementById('errorUser').style.display = 'block';
            userExists = true;
        } else {
            document.getElementById('username').style.borderColor = '#a1ffe9';
            document.getElementById('username').style.marginBottom = '14px';
            document.getElementById('errorUser').style.display = 'none';
            userExists = false;
        }
    }
</script>
<?php include('components/footer.php'); ?>
</body>
</html>