<?php
include('PHP/scripts/connection.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($_SESSION['user'])) {
    print $_SESSION['user'] . ' - ';
} ?>Home - KnowItAll</title>
    <link rel="stylesheet" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,
     maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi"/>
</head>

<body>
<?php include('PHP/pages/components/header.php'); ?>
<p class="headtext">Welkom op KnowItAll-techWeetjes!</p><br><br>
<div><p class="headweetje">Wist je dat?<br><br>
        <?php
        $date = strftime('%Y-%m-%d');
        $result = mysqli_query($conn, "SELECT * FROM facts WHERE date LIKE '$date%' ORDER BY RAND()");
        if ($result) {
            $numrows = mysqli_num_rows($result);
            if ($numrows !== 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $fact_text = $row['fact_text'];
                    $read_more = $row['read_more'];
                    $image = $row['image'];
                }

                if (isset($image)) {
                    ?>
            <img src="PHP/pages/<?= $image ?>"><br>
        <?php
                } ?>
        <?= $fact_text ?>
    <?php if (isset($read_more) && strlen($read_more) > 0) {
                    ?>
    <br><a href="<?= $read_more ?>" target="_blank">Lees Meer</a>
    <?php
                }
            }
        } else {
            echo mysqli_error($conn);
        }
    ?></p></div>
<?php include('PHP/pages/components/footer.php'); ?>
</body>
</html>
